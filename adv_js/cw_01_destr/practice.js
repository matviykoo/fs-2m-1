// TASK 1
/*
 * Две компании решили объединиться, и для этого им нужно объединить базу данных своих клиентов.
 * У вас есть 2 массива строк, в каждом из них - фамилии клиентов. Создайте на их основе один массив, который будет представлять * собой объединение двух массив без повторяющихся фамилий клиентов
 * */

const clients1 = ['Гилберт', 'Сальваторе', 'Пирс', 'Соммерс', 'Форбс', 'Донован', 'Беннет', 'Пирс']
const clients2 = ['Пирс', 'Зальцман', 'Сальваторе', 'Майклсон']

// const newDataBase = [...clients1, ...clients2];

// console.log('🚀 ==== > newDataBase', [...new Set(newDataBase)])

// Задание 2
// Перед вами массив characters, состоящий из объектов. Каждый объект описывает одного персонажа.
// Создайте на его основе массив charactersShortInfo, состоящий из объектов, в которых есть только 3 поля - name, lastName и age.

const characters = [
	{
		name: 'Елена',
		lastName: 'Гилберт',
		age: 17,
		gender: 'woman',
		status: 'human',
	},
	{
		name: 'Кэролайн',
		lastName: 'Форбс',
		age: 17,
		gender: 'woman',
		status: 'human',
	},
	{
		name: 'Аларик',
		lastName: 'Зальцман',
		age: 31,
		gender: 'man',
		status: 'human',
	},
	{
		name: 'Дэймон',
		lastName: 'Сальваторе',
		age: 156,
		gender: 'man',
		status: 'vampire',
	},
	{
		name: 'Ребекка',
		lastName: 'Майклсон',
		age: 1089,
		gender: 'woman',
		status: 'vempire',
	},
	{
		name: 'Клаус',
		lastName: 'Майклсон',
		age: 1093,
		gender: 'man',
		status: 'vampire',
	},
]

// const charactersShortInfo = []

// variant 1
// characters.forEach(item => {
//     const { name, lastName, age } = item;
// 	const shortUserInfo = {
//         name: name,
// 		lastName: lastName,
// 		age: age,
// 	}
//     charactersShortInfo.push(shortUserInfo)
// })
// variant 2
// characters.forEach(({ name, lastName, age }) => {
//     const shortUserInfo = {
//         name,
// 		lastName,
// 		age,
// 	}
// 	charactersShortInfo.push(shortUserInfo)
// })
// variant 3
// const charactersShortInfo = characters.map(({ name, lastName, age }) => ({
// 	name,
// 	lastName,
// 	age,
// }))

// console.log('🚀 ==== > charactersShortInfo ', charactersShortInfo)



// TASK 3 
// const user1 = {
// 	name: 'John',
// 	years: 30,
// }

// const {name, years: age, isAdmin = false} = user1;


// console.log(name, age, isAdmin);


// TASK 4   
const satoshi2020 = {
	name: 'Nick',
	surname: 'Sabo',
	age: 51,
	country: 'Japan',
	birth: '1979-08-21',
	location: {
		lat: 38.869422,
		lng: 139.876632,
	},
}

const satoshi2019 = {
	name: 'Dorian',
	surname: 'Nakamoto',
	age: 44,
	hidden: true,
	country: 'USA',
	wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
	browser: 'Chrome',
}

const satoshi2018 = {
	name: 'Satoshi',
	surname: 'Nakamoto',
	technology: 'Bitcoin',
	country: 'Japan',
	browser: 'Tor',
	birth: '1975-04-05',
}


const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020}
// const fullProfile = {}


// for (let key in satoshi2018) {
//     fullProfile[key] = satoshi2018[key]
// }
// for (let key in satoshi2019) {
//     fullProfile[key] = satoshi2019[key]
// }
// for (let key in satoshi2020) {
//     fullProfile[key] = satoshi2020[key]
// }
// console.log("🚀 ==== > fullProfile", fullProfile);
