/* ЗАДАЧА - 2
* Есть два массива пользователей. Одни - получили рекламу на фейсбуке, вторые - в поисковике.
* Пользователи обоих массивов кликнули по рекламе, по этому их нужно объединить в один массив - clickedLeads.
*/


const fbLeeds = [
    {name: 'John', age:23},
    {name: 'Mia', age:18},
    {name: 'Leo', age:70},
    {name: 'Ed', age:30},
];

const googleLeeds = [
    {name: 'Kiki', age:27},
    {name: 'Otto', age:20},
    {name: 'Ivan', age:31},
    {name: 'Bobbi', age:30},
];

const clickedLeads = [...fbLeeds, ...googleLeeds]
// const clickedLeads = fbLeeds.concat(googleLeeds)
// console.log("🚀 ==== > clickedLeads", clickedLeads);


// fbLeeds.forEach(item => {
//     clickedLeads.push(item)
// })
// googleLeeds.forEach(item => {
//     clickedLeads.push(item)
// })
// console.log(clickedLeads);































