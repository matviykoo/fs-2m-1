/* ЗАДАЧА - 4
 * Написать функцию, возвращаемым значением которой является объект простой кофемашины.
 * Функционал кофемашины:
 * 1 - хранит запасы продуктов в свойствах - milk, sugar, water
 * 2 - содержит метод getDrinkPrice(drinkName), где аргумнет drinkName содержить имя напитка, стоимость которого должен вернуть метод.
 */

function coffeeMachine() {
	const infoMachine = {
		milk: 1000,
		sugar: 1000,
		water: 1000,
        drinkPrice: {
            espresso: 20,
            cappuccino: 40
        },
		getEspresso() {
			alert('Вот ваш експресо')
		},
		getCappuccino() {
			alert('Вот ваш капучино')
		},

        getDrinkPrice(drinkName) {
            return this.drinkPrice[drinkName]
        }
	}

    return infoMachine
}

const result = coffeeMachine();
console.log('Price ==== > ', result.getDrinkPrice('cappuccino'))
