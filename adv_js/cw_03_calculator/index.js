// * ---------------- CHANGE COLOR THEME ----------------
const wrapperChangeTheme = document.querySelector('.change__theme')
const moon = document.getElementById('moon')
const sun = document.getElementById('sun')

window.addEventListener('load', () => {
	const storageData = localStorage.getItem('theme')
	document.body.className = storageData
	if (storageData === 'light-theme') {
		sun.classList.add('active')
	} else {
		moon.classList.add('active')
	}
})

wrapperChangeTheme.addEventListener('click', function ({ target }) {
	switch (target.id) {
		case 'sun':
			localStorage.setItem('theme', 'light-theme')
			moon.classList.remove('active')
			target.classList.add('active')
			break
		case 'moon':
			localStorage.setItem('theme', 'dark-theme')
			sun.classList.remove('active')
			target.classList.add('active')
			break
		default:
			null
	}
	document.body.className = localStorage.getItem('theme')
})
// * ---------------- CHANGE COLOR THEME ----------------

const data = {
	firstNum: '',
	secondNum: '',
	operation: null,
}

let count = 1

function ComponentKeyboard(content, name) {
	this.element = document.createElement('input')
	this.content = content
	this.name = name

	this.render = () => {
		this.element.type = 'submit'
		this.element.value = this.content
		this.element.name = this.name
		this.element.className = 'calculator-buttons'
		this.handleClick()
		return this.element
	}

	this.handleClick = () => {
		this.element.addEventListener('click', function (event) {
			const showNumbers = document.querySelector('.calculator-showNumbers')
			const oldValue = document.querySelector('.calculator-showNumbers-old')

			if (
				this.name !== 'back' &&
				this.name !== 'calc' &&
				this.name !== 'clear' &&
				this.name !== 'handleReverse'
			) {
				showNumbers.textContent += event.target.value
			}

			switch (this.name) {
				case 'helper':
					console.log('helpers')
					break
				case 'number':
					if (!!data.operation) {
						data.secondNum += this.value
					} else {
						data.firstNum += this.value
					}
					break
				case 'operation':
					if (!data.operation) {
						data.operation = this.value
					} else {
						let str = 'operation' + count
						data[str] = this.value
						count++
						console.log(data);
					}
					break
				case 'calc':
					let result = calcField(+data.firstNum, +data.secondNum, data.operation)
					showNumbers.textContent = parseFloat(result)
					oldValue.textContent = `${data.firstNum} ${data.operation} ${data.secondNum}`
					for (let key in data) {
						data[key] = ''
					}
					data.result = result
					break
				case 'back':
					const changeStringsValue = showNumbers.textContent.split('')
					changeStringsValue.pop()
					showNumbers.textContent = changeStringsValue.join('')
					break
				case 'clear':
					showNumbers.textContent = ''
					oldValue.textContent = ''
					for (let key in data) {
						data[key] = ''
					}
					break
				case 'handleReverse':
					// find bugs when taps with all operation calc
					if (showNumbers.textContent.includes('-')) {
						showNumbers.textContent = showNumbers.textContent.split('-').join('')
					} else {
						showNumbers.textContent = '-' + showNumbers.textContent
					}
					break
				default:
					break
			}

			// console.log(data)
			// console.log('value = ', this.value)
			// console.log('name = ', this.name)
		})
	}
}

const contentField = [
	{ value: 'AC', name: 'clear' },
	{ value: '+/-', name: 'handleReverse' },
	{ value: '%', name: 'helper' },
	{ value: '/', name: 'operation' },
	{ value: 7, name: 'number' },
	{ value: 8, name: 'number' },
	{ value: 9, name: 'number' },
	{ value: 'x', name: 'operation' },
	{ value: 4, name: 'number' },
	{ value: 5, name: 'number' },
	{ value: 6, name: 'number' },
	{ value: '-', name: 'operation' },
	{ value: 1, name: 'number' },
	{ value: 2, name: 'number' },
	{ value: 3, name: 'number' },
	{ value: '+', name: 'operation' },
	{ value: 'back', name: 'back' },
	{ value: 0, name: 'number' },
	{ value: '.', name: 'helper' },
	{ value: '=', name: 'calc' },
]

contentField.forEach(item => {
	const { value, name } = item
	const field = new ComponentKeyboard(value, name)
	const fieldElement = field.render()
	document.getElementById('calculator-keyboard-items').append(fieldElement)
})

function calcField(firstNum, secondNum, operation) {
	switch (operation) {
		case '-':
			return firstNum - secondNum
			break
		case '+':
			return firstNum + secondNum
			break
		case '/':
			return firstNum / secondNum
			break
		case 'x':
			return firstNum * secondNum
			break
		default:
			return null
	}
}
