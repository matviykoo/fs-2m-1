let container, camera, renderer, scene, unit

function init(path) {
	container = document.querySelector('.scene')

	// Create scene
	scene = new THREE.Scene()

	const fov = 20
	const aspect = container.clientWidth / container.clientHeight
	const near = 0.1
	const far = 500

	camera = new THREE.PerspectiveCamera(fov, aspect, near, far)

	camera.position.set(0, 0, 20)

	const ambient = new THREE.AmbientLight(0x404040, 2)
	scene.add(ambient)


    const light = new THREE.DirectionalLight(0xffffff, 2)

    light.position.set(10,10,10);
    scene.add(light)

	// Render
	renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true })
	renderer.setSize(container.clientWidth, container.clientHeight)
	renderer.setPixelRatio(window.devicePixelRatio)

	container.append(renderer.domElement)

	//Load model
	let loader = new THREE.GLTFLoader()
	loader.load(path, function (gltf) {
		scene.add(gltf.scene)
		unit = gltf.scene.children[0]
        animate()
		// renderer.render(scene, camera)
	})
}

function animate() {
	requestAnimationFrame(animate)

	unit.rotation.z  += 0.01
	renderer.render(scene, camera)
}

// init('./model/air/scene.gltf')
init('./model/dron/scene.gltf')


function onWindowResize () {
    camera.aspect = container.clientWidth / container.clientHeight

    camera.updateProjectMatrix()
    renderer.setSize(container.clientWidth, container.clientHeight)
}

window.addEventListener('resize', onWindowResize)