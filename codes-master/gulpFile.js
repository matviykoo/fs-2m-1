const gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    minifyjs= require('gulp-js-minify'),
    cleaner = require('gulp-clean'),
    cleanCss = require('gulp-clean-css'),
    sass = require('gulp-sass');


const path = {
    src: {
        html: 'src/*.html',
        scss: 'src/style/**/*.scss',
        img: 'src/img/**/**',
        js: 'src/js/*.js',
    },
    dist: {
        html: 'dist',
        css: 'dist/css',
        img: 'dist/img',
        js: 'dist/js',
    },
}

const htmlBuild = () => gulp.src(path.src.html)
    .pipe(gulp.dest(path.dist.html))
    .pipe(browserSync.stream());
 
const scssBuild = () => gulp.src(path.src.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
    .pipe(gulp.dest(path.dist.css))
    .pipe(browserSync.stream())
    

const imgBuild = () => gulp.src(path.src.img)
    .pipe(imagemin())
    .pipe(gulp.dest(path.dist.img));

const jsBuild = () => gulp.src(path.src.js)
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(minifyjs())
    .pipe(gulp.dest(path.dist.js))
    .pipe(browserSync.stream());

const clean = () => gulp.src(path.dist.html, { allowEmpty : true })
    .pipe(cleaner())
    .pipe(cleanCss({ Compatibility: 'ie8' }))

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: './dist',
        },
    }),
    gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload)
    gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload)
    gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload)
}


gulp.task('default', gulp.series(clean, htmlBuild, scssBuild, jsBuild, imgBuild, watcher));  
